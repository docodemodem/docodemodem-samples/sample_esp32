var da_val;
var red_led;
var green_led;
var gout_val;

function beep_process() {
    let url = "/beep"

    var postdata = {
        "device_id": device_id,
    };

    //console.log(postdata);

    // 制御通信実行
    $.ajax({
        type: "post",
        url: url,
        data: JSON.stringify(postdata),
        contentType: 'application/json',
        dataType: "json",
        success: function (json_data) {
            //console.log(json_data);
        },
        error: function () {
            //console.log("Server Error. Pleasy try again later.");
        },
        complete: function () {
        }
    });
}

function control_process() {

    let url = "/control"

    var postdata = {
        "da_val": da_val,
        "red_led": red_led,
        "green_led": green_led,
        "gout_val": gout_val
    };

    //console.log(postdata);

    // 制御通信実行
    $.ajax({
        type: "post",
        url: url,
        data: JSON.stringify(postdata),
        contentType: 'application/json',
        dataType: "json",
        success: function (json_data) {
            //console.log(json_data);
        },
        error: function () {
            //console.log("Server Error. Pleasy try again later.");
        },
        complete: function () {
        }
    });
}