var device_id = 0;
var temp = 0;
var press = 0;
var humi = 0;
var wind = 0;
var sndrssi = 0;
var rcvrssi  = 0;

$(document).ready(function(){
	sensor_process();
 
	$('#stopex').click(function (e) {
		beep_process();
	});

	setInterval("sensor_process()", 3000);

});

function sensor_process(){

	let url = "/sensors"

	$.ajax({
		type: "get",
		url: url,
		data: null,
		contentType: 'application/json',
		dataType: "json",
		success: function(data) {
			//console.log(data);
			device_id = data.device_id;
			temp = data.temp;
			press = data.press;
			humi = data.humi;
			wind = data.wind;
			sndrssi = data.sndrssi;
			rcvrssi = data.rcvrssi;

			// アナログデータの表示 */
			$("#device_id").text(device_id);
			$("#temp").text(temp.toFixed(1));
			$("#press").text(press.toFixed(1));
			$("#humi").text(humi.toFixed(1));
			$("#wind").text(wind.toFixed(1));
			$("#sndrssi").text(sndrssi.toFixed(0));
			$("#rcvrssi").text(rcvrssi.toFixed(0));
		},
		error: function () {
			$("#device_id").text("###");
			$("#temp").text("#.##");
    		$("#temp").text("#.##");
			$("#press").text("#.##");
			$("#humi").text("#.##");
			$("#wind").text("#.##");
			$("#sndrssi").text("#.##");
			$("#rcvrssi").text("#.##");
		},
		complete: function() {
		}
    });
}

