# どこでもでむ用のサンプルプログラムです。

データを受信するとWiFi経由でデータをブラウザで表示することができます。

ブラウザでbeepボタンを押すと、次のデータ受信のタイミングでbeep要求を送信します。

どこでもでむminiのサンプルプログラムとセットで使ってください。

https://gitlab.com/docodemodem/docodemodem-samples/sample_m0


# Know-How

必要なライブラリはPlatformIOに読み込むと自動でダウンロードされますが、少々時間がかかります。

PlatformIOのターミナルを開き、下記コマンドを実行してエラーがなくなればOKです。

> pio lib install
<br>
<br>
dataフォルダにHTML関連のファイルがあるので、「どこでもでむ」のSPIFFSへ転送してください。

PlatformIOのターミナルで次のコマンドを実行すると転送されます。

> platformio run --target uploadfs

