
/*
 * Sample program for DocodeModem
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#ifndef _HTTPSERVER_H_
#define _HTTPSERVER_H_

/*======================================================================
 *  Include File 
 *====================================================================*/
#include <Arduino.h>
#include "Docodemo.h"

/*======================================================================
 *	#define for Global
 *====================================================================*/

void run_http_task();

#endif // _HTTPSERVER_H_
