/*
 * Sample program for DocodeModem
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include "httpserver.h"

#include <ESPmDNS.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

#include <stdio.h>
#include <string>
#include "AsyncJson.h"
#include "ArduinoJson.h"
#include <FS.h>

//#define USE_SD

#ifdef USE_SD
#include <SD.h>
#else
#include <SPIFFS.h>
#endif

const char *ssid = "********";
const char *password = "********";

const char *ap_ssid = "ESP32ap";
const char *ap_password = "12345678";

IPAddress ap_ip(192, 168, 100, 1);
IPAddress ap_gateway(192, 168, 100, 1);
const IPAddress ap_subnet(255, 255, 255, 0);

AsyncWebServer server(80);

extern float recvdata[6];
extern int16_t rssi;
extern bool sendBeepReq;
extern uint8_t targetId;

static String retSensorJson(void)
{
    DynamicJsonDocument doc(128);
    doc["device_id"] = ((int)recvdata[0]) & 0xff;
   
    doc["wind"] = recvdata[1];
    doc["temp"] = recvdata[2];
    doc["press"] = recvdata[3];
    doc["humi"] = recvdata[4];
    doc["sndrssi"] = (int)recvdata[5];
    doc["rcvrssi"] = rssi;

    String result;
    serializeJson(doc, result);
    //SerialDebug.println(result);
    return result;
}

void http_init()
{

#ifdef USE_SD
    //if (SD.begin())
    if (SD.begin(SS, SPI,400000,"/sd",8))
    {
        SerialDebug.println("SD Card initialized.");
    }
#else
    SPIFFS.begin();
#endif

    WiFi.disconnect(true);
    delay(100);
    
#if 0
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        SerialDebug.print(".");
        delay(1000);
    }
    SerialDebug.print("HTTP Server: http://");
    SerialDebug.print(WiFi.localIP());
    SerialDebug.println("/");
#else
    WiFi.softAPConfig(ap_ip, ap_gateway, ap_subnet);
    WiFi.softAP(ap_ssid, ap_password); //ssid password
    //WiFi.softAP(ap_ssid, ap_password,14,0,1);//ssid password channel(1,6,11,14) hidden maxconnection(1-4)

    delay(100);

    IPAddress myIP = WiFi.softAPIP();
    SerialDebug.print("AP IP address: ");
    SerialDebug.println(myIP);
#endif

#if 1
    if (MDNS.begin("esp32"))
    {
        MDNS.addService("http", "tcp", 80);
    }
#endif

    server.on("/sensors", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, F("application/json"), retSensorJson());
    });

    server.onRequestBody(
        [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
            if ((request->url() == "/control") && (request->method() == HTTP_POST)) 
            {
                //SerialDebug.println("/control");
                const size_t JSON_DOC_SIZE = 512;
                DynamicJsonDocument jsonDoc(JSON_DOC_SIZE);

                if (DeserializationError::Ok == deserializeJson(jsonDoc, (const char *)data))
                {
                    JsonObject obj = jsonDoc.as<JsonObject>();
                    int da_val = obj["da_val"].as<int>();
                    int red_led = obj["red_led"].as<int>();
                    int green_led = obj["green_led"].as<int>();
                    int gout_val = obj["gout_val"].as<int>();

                    SerialDebug.printf("control %d,%d,%d,%d\n", da_val, red_led, green_led, gout_val);

                    request->send(200, "application/json", "{ \"status\": \"OK\" }");
                }else{
                    request->send(400, "application/json", "{ \"status\": \"NG\" }");
                }
            }
            else if ((request->url() == "/beep") && (request->method() == HTTP_POST)) {
                //SerialDebug.println("/beep");
                const size_t JSON_DOC_SIZE = 512;
                DynamicJsonDocument jsonDoc(JSON_DOC_SIZE);

                if (DeserializationError::Ok == deserializeJson(jsonDoc, (const char *)data))
                {
                    JsonObject obj = jsonDoc.as<JsonObject>();
                    uint8_t device_id = obj["device_id"].as<uint8_t>();

                    SerialDebug.printf("beep device_id %d\n", device_id);

                    sendBeepReq = true;
                    targetId = device_id;

                    request->send(200, "application/json", "{ \"status\": \"OK\" }");
                    SerialDebug.println("Request Ok");
                }
                else
                {
                    request->send(400, "application/json", "{ \"status\": \"NG\" }");
                }
            }
        });

#ifdef USE_SD
    server.serveStatic("/", SD, "/").setDefaultFile("index.html");
#else
    server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");
#endif
    server.onNotFound([](AsyncWebServerRequest *request) {
        request->send(404);
    });

    server.begin();
}

TaskHandle_t Handle_thread_http;
static void thread_http(void *pvParameters)
{
    SerialDebug.println("Start http_task");
    http_init();

    while (1)
    {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

void run_http_task()
{
    xTaskCreatePinnedToCore(thread_http, "thread_http", 8192, NULL, 2, &Handle_thread_http, 0);
}