/*
 * Sample program for DocodeModem
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include <docodemo.h>
#include <SlrModem.h>
#include "httpserver.h"

#define CMD_HEADER 0x01
//#define USE_DISPLAY

const uint8_t DEVICE_EI = 0x01;
const uint8_t DEVICE_GI = 0x02;
const uint8_t DEVICE_DI = 0x00; //00 is boradcast
const uint8_t CHANNEL = 0x10;

DOCODEMO Dm;
HardwareSerial UartModem(MODEM_UART_NO);
SlrModem modem;

#ifdef USE_DISPLAY
#include <SSD1306.h>
#endif

static xTaskHandle ledblink_task_handle;
static void led_task(void *val)
{
  Serial.println("led_task start");

  while (1)
  {
    Dm.LedCtrl(GREEN_LED, ON);
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    Dm.LedCtrl(GREEN_LED, OFF);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }

  vTaskDelete(NULL);
}

int16_t rssi{0};
float recvdata[6];
bool sendBeepReq = false;
uint8_t targetId = 0;

bool sendBeep()
{
  uint16_t msg[2];

  msg[0] = 0x02;
  msg[1] = (uint16_t)targetId;

  auto rc = modem.TransmitData((uint8_t *)msg, sizeof(msg));
  if (rc == SlrModemError::Ok)
  {
    return true;
  }

  return false;
}

static xTaskHandle radio_task_handle;
static void radio_task(void *val) 
{

#ifdef USE_DISPLAY
  SSD1306 display(0x3c, EXTERNAL_SDA, EXTERNAL_SCL, GEOMETRY_128_64, I2C_TWO, 400000);
  display.setI2cAutoInit(true);
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_16);
  
  display.clear();
  display.drawString(0, 0, "Hello Wolrd");
  display.display();
  display.setI2cAutoInit(false);
#endif

  Dm.ModemPowerCtrl(ON);
  delay(150);

  UartModem.begin(19200, SERIAL_8N1, MODEM_UART_RX_PORT, MODEM_UART_TX_PORT);
  while (!UartModem)
    ;
  modem.Init(UartModem, nullptr);

  //if you turn off radio power, set again or select true to save setting.
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);

  while(1){
    modem.Work();

    if (modem.HasPacket())
    {
      Dm.LedCtrl(RED_LED, ON);
      SerialDebug.print("Received packet:");

      const uint8_t *pData;
      uint8_t len{0};
      modem.GetPacket(&pData, &len);
      modem.GetRssiLastRx(&rssi);

      if (len == 24) //size check
      {
        memcpy(&recvdata[0], pData, len);

        //simple packet header check
        uint8_t cmd = (((int)recvdata[0]) >> 8) & 0xff;
        if (cmd == CMD_HEADER)
        {
          SerialDebug.printf(" %.1fm/S, %.1fC, %.1fhPa, %.1f, env rssi: %d, rcv rssi: %d\r\n", recvdata[1], recvdata[2], recvdata[3], recvdata[4], (int)recvdata[5], rssi);
        }
        else
        {
          SerialDebug.println("Not our packet...");
          memset(&recvdata[0], 0, len);
        }
      }

      modem.DeletePacket();
      Dm.LedCtrl(RED_LED, OFF);

      if (sendBeepReq)
      {
        sendBeepReq = false;
        if (sendBeep())
        {
          SerialDebug.println("Beep packet Send ok");
        }
        else
        {
          SerialDebug.println("Beep packet Send Ng...");
        }
      }

#ifdef USE_DISPLAY
      display.clear();
      display.drawString(0, 20, String(rssi));
      display.drawString(35, 20, "dBm");
      display.drawString(0, 40, String(recvdata[1], 1));
      display.drawString(35, 40, "m/S");
      display.display();
#endif
    }
  }
  vTaskDelete(NULL);
}

void setup()
{
  SerialDebug.begin(115200);
  Dm.begin();

  xTaskCreatePinnedToCore(led_task, "ledblink_task", 8192, NULL, 2, &ledblink_task_handle, 1);
  xTaskCreatePinnedToCore(radio_task, "radio_task", 8192, NULL, 2, &radio_task_handle, 1);

  run_http_task();

  vTaskDelete(NULL);
}

void loop()
{
  // Never get here
}
